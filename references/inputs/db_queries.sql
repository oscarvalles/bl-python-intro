SELECT *
FROM employees 
WHERE TRUE 
AND hire_date BETWEEN '1999-10-01' AND '2000-01-01'
LIMIT 1000 
;

SELECT s.* 
FROM salaries s
INNER JOIN employees e
 ON s.emp_no = e.emp_no
WHERE TRUE 
AND hire_date BETWEEN '1999-10-01' AND '2000-01-01'
;